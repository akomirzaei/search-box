export function mapAreaList(areas) {
  const results = areas.map((item, index) => {
    return {
      ...item,
      id: index,
      selected: false,
    };
  });

  return results;
}
