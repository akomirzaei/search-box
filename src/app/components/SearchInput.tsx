import { useRef } from "react";

interface SearchInputProps {
  clearInput: () => void;
  clearList: () => void;
  clearable: boolean;
  cityTitle: string;
  placeholder?: string | null;
  value?: string;
}

export default function SearchInput({
  clearInput,
  clearList,
  ...props
}: SearchInputProps) {
  const searchInputRef = useRef<HTMLInputElement>(null);

  const handleClearInput = () => {
    clearInput();
    if (searchInputRef.current) {
      searchInputRef.current.blur();
    }
  };

  const handleClearList = () => {
    clearList();
    if (searchInputRef.current) {
      searchInputRef.current.blur();
    }
  };

  return (
    <div className="search-input-wrapper bg-white p-2 sticky top-0 z-10">
      {!props.placeholder && !props.value && (
        <label
          htmlFor="search-input"
          className="search-input-title text-sm absolute"
        >
          <span>جستجو در </span>
          <span className="text-fuchsia-600">شهر {props.cityTitle}</span>
        </label>
      )}

      <input
        id="search-input"
        type="text"
        ref={searchInputRef}
        className="w-full h-10 px-10 text-sm text-right rounded-md bg-gray-100 text-gray-700"
        {...props}
      />

      {props.clearable && (
        <i
          className="icon-close search-button__remove"
          onClick={handleClearList}
        />
      )}

      {props.value && (
        <i
          className="icon-arrow-right search-button__arrow"
          onClick={handleClearInput}
        />
      )}

      {!props.value && <i className="icon-search search-button__search" />}
    </div>
  );
}
