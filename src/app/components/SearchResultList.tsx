import { SearchResultListProps, Area } from "@/app/types/index";

export default function SearchResultList({
  results,
  updateList,
}: SearchResultListProps) {
  const handleSelectArea = (area: Area): void => {
    updateList(area);
  };

  return (
    <div className="search-list-wrapper mt-2 bg-white">
      {results.map((area: Area, index: number) => {
        return (
          <div
            key={index}
            className="search-list-item radio-button text-sm text-gray-500 border-b last:border-b-0"
          >
            <label className="flex items-center justify-between grow p-3 ">
              {area.title}
              <input
                type="radio"
                value={area.title}
                checked={area.selected}
                onChange={() => handleSelectArea(area)}
              />
            </label>
          </div>
        );
      })}
    </div>
  );
}
