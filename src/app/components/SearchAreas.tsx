"use client";

import { useState, ChangeEvent } from "react";
import { FilterAreaProps, Area } from "@/app/types/index";
import { mapAreaList } from "@/app/tools/mapper";
import { TEHRAN_AREAS } from "@/app/constant/tehranArea";
import SearchInput from "@/app/components/SearchInput";
import SearchResultList from "@/app/components/SearchResultList";

export default function SearchAreas() {
  const [searchTitle, setSearchTitle] = useState<string>("");

  const [showPlaceholder, setShowPlaceholder] = useState<boolean>(true);

  const [areaList, setAreaList] = useState(mapAreaList(TEHRAN_AREAS));

  const [isClearable, setIsClearable] = useState<boolean>(false);

  const searchAreas = ({ tehranAreas, searchTitle }: FilterAreaProps) => {
    return tehranAreas.filter((item: Area) => item.title.includes(searchTitle));
  };

  const handleSearchResults = searchAreas({
    tehranAreas: areaList,
    searchTitle,
  });

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchTitle(event.target.value);
  };

  const handleSelectAreas = (selectedItem : Area) => {
    const updatedAreas = areaList.map((item : Area) => {
      if (item.id === selectedItem.id) {
        return { ...item, selected: true };
      }
      return item;
    });

    setAreaList(updatedAreas);

    if (!isClearable) {
      setIsClearable(true);
    }
  };

  const handleClearList = () => {
    const updatedAreas = areaList.map((item : Area) => {
      if (item.selected) {
        return { ...item, selected: false };
      }
      return item;
    });

    setAreaList(updatedAreas);

    setIsClearable(false);
  };

  const handleCleanInput = () => {
    setSearchTitle("");
  };

  const handleShowPlaceHolder = () => {
    setShowPlaceholder(true);
  };

  const handleHidePlaceHolder = () => {
    setShowPlaceholder(false);
  };

  const placeholder = showPlaceholder ? null : "جستجو";

  const cityTitle = "تهران";

  return (
    <>
      <SearchInput
        value={searchTitle}
        placeholder={placeholder}
        cityTitle={cityTitle}
        onChange={handleInputChange}
        onFocus={handleHidePlaceHolder}
        onBlur={handleShowPlaceHolder}
        clearInput={handleCleanInput}
        clearList={handleClearList}
        clearable={isClearable}
      />

      <SearchResultList
        results={handleSearchResults}
        updateList={handleSelectAreas}
      />
    </>
  );
}
