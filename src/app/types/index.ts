export interface Area {
  id: number;
  title: string;
  selected: boolean;
}

export interface SearchResultListProps {
  results: Area[];
  updateList: (area: Area) => void;
}

export interface FilterAreaProps {
  tehranAreas: Area[];
  searchTitle: string;
}
